/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobscraper.statistics;

import java.util.Date;
import java.util.TreeMap;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author tushar
 */
public class ParseStatics {

    private static TreeMap<String, Integer> jobConunter = new TreeMap<String, Integer>();
    private static Logger logger = LogManager.getLogger(ParseStatics.class.getName());

    public static TreeMap<String, Integer> getJobConunter() {
        return jobConunter;
    }

    public static void setJobConunter(String webSiteName, int jobCount) {

        if (!jobConunter.containsKey(webSiteName)) {
            jobConunter.put(webSiteName, 0);
        }
        try {
            jobConunter.put(webSiteName, jobCount + jobConunter.get(webSiteName));
        } catch (Exception e) {
            logger.error(e);
        }

    }

    public static void clearBookConunter() {
        jobConunter.clear();
    }

    public static void sendStatics() {

        EmailNotifier mailnotification = new EmailNotifier();
        if (mailnotification.SendEmail("Current Time is: " + new Date() + "\n" + ParseStatics.getJobConunter().toString().replace(",", "\n"))) {
            clearBookConunter();
        }
    }

}
