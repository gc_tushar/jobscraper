package jobscraper.statistics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import jobscraper.common.Configuration;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tushar
 */
public class EmailNotifier {

    private final String email_sender = Configuration.getConfiguration("EMAIL_SENDER_ADDRESS");;
    private final String email_sender_password = Configuration.getConfiguration("EMAIL_SENDER_PASSWORD");;
    private static final String email_receivers = Configuration.getConfiguration("EMAIL_RECEIVERS");

    public boolean SendEmail(String errorMessage) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email_sender, email_sender_password);
            }
        });

        try {
            
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email_sender));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email_receivers));
            message.setSubject("Job Scraper Statistics");
            message.setText(errorMessage);

            Transport.send(message);
            
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    private static String loadMailAddressFromFile() {
        String emailAddresses = "";

        try {
            File emailFile = new File(Configuration.READER_RESOURCE_LOCATION + "/email.txt");
            FileReader fileReader = new FileReader(emailFile);
            BufferedReader reader = new BufferedReader(fileReader);
            while (reader.ready()) {
                String emails = reader.readLine();
                emailAddresses = emailAddresses + emails + ",";
            }
            fileReader.close();
            reader.close();
            emailFile = null;
        } catch (Exception e) {
            return "";
        }
        return emailAddresses;
    }

    public static void main(String[] args) {
        EmailNotifier mailnotification = new EmailNotifier();
        mailnotification.SendEmail("testing");
    }
}